A simple interface built using Reactjs. This application consumes the [Thinkific Increment Integer API
 ](https://gitlab.com/susanesho/thinkific-coding-challenge). With this application you can register as a user and easily increment values and retrieve current values. 
 Please note you must be registered in other to view current_value or next_value
