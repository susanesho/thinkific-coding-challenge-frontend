import React, { Component } from 'react';
import './App.css';
import './SimpleForm.css';

import TextInput from './components/TextInput'
import { Button } from'react-bootstrap'
import { MakeRequest } from './MakeRequest'
import { withRouter } from 'react-router-dom';
import toastr from 'toastr'

class App extends Component {
	state = {email: " ", password: "" };

	componentDidMount() {
		const token = localStorage.getItem('token');
		if (token) {
			console.log(token);
			this.props.history.push('/v1/current');
			toastr.error("Already registered")
		}
 	}

	handleSubmit = async () => {
		await MakeRequest("users", "POST", {email: this.state.email, password: this.state.password})
      .then((response)=> {
				if(response.token){
					localStorage.setItem('token', response.token);
					this.props.history.push('/v1/next');
				} else {
					let errors = response.errors.map(error => error.detail);
					toastr.error(errors.join(', '));
				}
			});
	};
	handleInput = (e) => {
		e.preventDefault();
		this.setState({[e.target.name]: e.target.value})
	};
  render() {
    return (
      <div className="App">
        <header className=""> </header>
				<div className="wrapper">
					<div className="inner">
						<div className="custom-container">
							<div className="form-wrapper">
								<h3>Register</h3>
								<TextInput name="email" label="Email" type="email" value={this.state.email} placeholder="science@gmail.com" onChange={this.handleInput} />
								<TextInput name="password" label="Password" type="password" value={this.state.password} placeholder="password" onChange={this.handleInput} />
								<Button type="submit" onClick={this.handleSubmit}>Submit</Button>
							</div>
						</div>
					</div>
				</div>
      </div>
    );
  }
}

export default withRouter(App);
