export async function MakeRequest(endpoint, method, data, token=""){
	return fetch(
		`https://thinkific-coding-challenge.herokuapp.com/v1/${endpoint}`,
		{
			method: method,
			headers: {
				"Content-Type": "application/json",
				"Authorization": token,
			},
			body: JSON.stringify(data)
		}
	)
		.then((response)=> {
			return response.json();
		})
}

export async function MakeGETRequest(endpoint, token){
	return fetch(
		`https://thinkific-coding-challenge.herokuapp.com/v1/${endpoint}`,
		{
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				"Authorization": token,
			}
		}
	)
		.then((response)=> {
			return response.json();
		})
}