import React from 'react'
import { MakeGETRequest, MakeRequest } from '../MakeRequest'
import { withRouter } from "react-router-dom";
import { Button } from'react-bootstrap';
import TextInput from '../components/TextInput';
import toastr from 'toastr'

class CurrentInteger extends React.Component {
	state = { value: null };
	componentDidMount() {
		const token = localStorage.getItem('token');
		if (token) {
			MakeGETRequest("current", token).then(response => {
				this.setState({ value: response.data.attributes.value });
			});
		} else {
			this.props.history.push('/');
			toastr.error("Unathorized")
		}
	}

	handleChange = e => {
		this.setState({ value: e.target.value })
	};

	handleSubmit = async () => {
		await MakeRequest("current", "PUT", {current: this.state.value}, localStorage.getItem('token')).then(response => {
			console.log(response);
			if(response.data){
				console.log(response.data);
				toastr.success(`current Integer reset to ${response.data.attributes.value}`);
			} else {
				toastr.error(response.errors.map(error => error.detail));
			}
		});
	};

	handleSignout = () =>{
		localStorage.removeItem("token");
		this.props.history.push('/')
	};

	render() {
		return (
			<div className="CurrentInteger">
				<header className=""> </header>
				<div className="wrapper">
					<div className="inner">
						<div className="custom-container">
							<div className="form-wrapper">
								<TextInput name="CurrentInteger" label="CurrentInteger" type="number" value={this.state.value} placeholder="CurrentNum" onChange={this.handleChange} />
								<Button type="submit" onClick={this.handleSubmit}>Submit</Button>
								<div className="signout-span">
									<h6 onClick={this.handleSignout}>Sign Out?</h6>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
export default withRouter(CurrentInteger);