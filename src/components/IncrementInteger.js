import React from 'react'
import { MakeGETRequest } from '../MakeRequest'
import {withRouter} from "react-router-dom";
import {Button} from "react-bootstrap";
import toastr from "toastr";

class IncrementInteger extends React.Component {
	state = { value: null };

	componentDidMount() {
		const token = localStorage.getItem('token');
		if (token) {
			MakeGETRequest("next", token).then(response => {
				this.setState({ value: response.data.attributes.value });
			});
		} else {
			this.props.history.push('/');
			toastr.error("Unathorized")
		}
	}

	handleSignout = () =>{
		localStorage.removeItem("token");
		this.props.history.push('/')
	};

	render() {
		return(
			<div className="wrapper">
				<div className="inner">
					<div className="custom-container">
						<div className="form-wrapper">
							<div className="CurrentInteger">
								<header className=""> </header>
									<h4> Your current Integer is {this.state.value} </h4>
								<div className="signout-span">
									<Button type="signout" onClick={this.handleSignout}>signout</Button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
export default withRouter(IncrementInteger);