import React from 'react';
import {Form} from 'react-bootstrap'

export default ({name, label, type, onChange, value, placeholder}) => {
	return (
		<Form.Group
			controlId="formBasicText"
		>
			<Form.Label>{label}</Form.Label>

			<Form.Control
				name={name}
				type={type}
				value={value}
				placeholder={placeholder}
				onChange={onChange}
			/>
			<Form.Control.Feedback />
		</Form.Group>
	);
}