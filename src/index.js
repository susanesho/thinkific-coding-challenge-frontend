import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import IncrementInteger from './components/IncrementInteger';
import CurrentInteger from './components/CurrentInteger';
import 'bootstrap/dist/css/bootstrap.css';
import 'toastr/build/toastr.min.css';
import * as serviceWorker from './serviceWorker';

const routing = (
	<Router>
		<div>
			<nav className="navbar navbar-expand-lg navbar-light bg-white">
				<div className="collapse navbar-collapse" id="navbarSupportedContent">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item active">
							<Link className="nav-link" to="/"><span className="sr-only">(current)></span>Home</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" to="/v1/next">Next Integer</Link>
						</li>
						<li>
							<Link className="nav-link" to="/v1/current">CurrentInteger</Link>
						</li>
					</ul>
				</div>
			</nav>
			<Route exact path="/" component={App} />
			<Route path="/v1/next" component={IncrementInteger}  />
			<Route path="/v1/current" component={CurrentInteger} />
		</div>
	</Router>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
